/*  
@name= index.js
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= controller associated to index.html
  @date = 4-11-2019
  @params= none 
  @return = none 
*/
window.onload = function () {
    document.getElementById("picture").style.display = "block";
    document.getElementById("frame").style.display = "block";
  
}
