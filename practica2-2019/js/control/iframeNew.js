/*  
@name= iframeNew.js
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= controller associated to iframeNew.html
  @date = 4-11-2019
  @params= none 
  @return = none 
*/
var str_letter = "";
var arr_valid_letters = ["A", "G", "C", "T"];
var arr_valid_protein = ["F", "L", "S", "Y", "*", "C", "W", "P", "H", "Q", "R", "I", "M", "T", "N", "K", "S", "V", "A", "D", "E", "G"];

/*  
@name= dataOnload
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= event that captured when the pag is loades. It's used to hide/show some components
  @date = 4-11-2019
  @params= none 
  @return = none 
*/
function dataOnload() {
    document.getElementById("enterProducts").style.display = "none";
}
/*  
@name= introduce
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the introduce button. 
                Create the rows according to the number of products entered.
  @date = 4-11-2019
  @params= none 
  @return = none 
*/
function introduce() {

    document.getElementById("divSelect").style.display = "none";
    document.getElementById("divNumProduct").style.display = "none";
    document.getElementById("enterProducts").style.display = "block";

    var selectType = document.getElementById("codeType").value;
    document.getElementById("titleSelectProduct").innerText = "Enter Products for Category " + selectType;


    var numberProducts = document.getElementById("numberProperties").value;
    let i;
    let index = 0;

    var rowCode = "";
    for (i = 0; i < numberProducts; i++) {
        index = i;

        rowCode += "<tr id='" + index + "'>" +
            " <td><input type='text' placeholder='Product name' id='" + i + 0 + "'></input> </td>" +
            "<td><input type='text' placeholder='Code' id='" + i + 1 + "'></input> </td>" +
            "<td> <input type='checkbox' id='" + i + 2 + "'></td>" +
            "</tr>";
    };
    document.getElementById("mytable").innerHTML += rowCode;

}

/*  
@name= validate_letter
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= Validate if a letter in arr_valid_letters or arr_valid_protein, is called by function validate_letters_code
  @date = 4-11-2019
  @params= str_letter , arr_valid_letters
  @return = bool
*/

function validate_letter(str_letter, arr_valid_letters) {
    var vLen = arr_valid_letters.length;
    var i;
    for (i = 0; i < vLen; i++) {
        if (str_letter == arr_valid_letters[i]) {
            return true;
        }
    }
    return false;
}

/*  
@name= validate_letters_code
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= Check that each letter of the code, column 2 of the table, is in the corresponding array.
                Validate the DNA code is nitrogenous base: A, G, C, T.
                Validate the Protein code is one of the letters of the array_letters_protein.
                Is called by function summarize()
  @date = 4-11-2019
  @params= str_letter , arr_valid_letters
  @return = bool
*/

function validate_letters_code() {
    var selectType = document.getElementById("codeType").value;
    var numberProd = document.getElementById("numberProperties").value;
    var i;
    for (i = 0; i < numberProd; i++) {
        var strCode = document.getElementById(i + "1").value;
        strCode = strCode.toUpperCase();
        var p1 = "";
        var p2 = "";
        var pError = "";
        p1 = "Row " + i + " " + strCode;
        var sLen = strCode.length;
        var index;
        var letter;
        for (index = 0; index < sLen; index++) {
            letter = strCode[index];
            if (selectType == "DNA code") {
                if (validate_letter(letter, arr_valid_letters) == false) {
                    alert("You have errors, correct them please <^_*>");
                    p2 = letter + "-It is not a nitrogen base";
                    pError += " <p> " + p1 + "</p>" + "<p> " + p2 + "</p>";

                }
            } else {
                if (validate_letter(letter, arr_valid_protein) == false) {
                    alert("You have errors, correct them please <^_*>");
                    p2 = letter + "-It is not a protein";
                    pError += " <p> " + p1 + "</p>" + "<p> " + p2 + "</p>";

                }
            }
        }
        document.getElementById("divError").innerHTML += pError;
    }
}
/*  
@name= summarize
  @author= Estefani Paredes Valera
  @version= 1.0
  @description=This method is triggered when the user click the Introduce_the_products_in_the_Data_Base botton
               Call validate_letters_code
  @date = 4-11-2019
  @params= str_letter , arr_valid_letters
  @return = bool
*/
function summarize() {

    validate_letters_code();

    var decision = confirm("Do you really want to introduce this products?");
    if (decision) {
        window.open("../popUpWindows/popUpWindow.html", "blank", "width=400px, height=400px");
    }
}


