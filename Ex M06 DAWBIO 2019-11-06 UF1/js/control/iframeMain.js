/*
@name= window.onload
@author= 
@version= 1.0
@description= Function that when you start the page shows the iframe
@date = 11-11-2018
@params= none
@return = none
*/

window.onload = function () {
    goBack();
    //document.getElementById("number").innerHTML = "";
}

/*
@name= goBack
@author= 
@version= 1.0
@description= Function that only shows the main menu
@date = 11-11-2018
@params= none
@return = none
*/
function goBack() {
    window.parent.document.getElementById("divPrincipal").style.display = "block";
    window.parent.document.getElementById("frame").style.display = "none";
    document.getElementById("number").innerHTML = "";
    window.parent.document.getElementById("numberClients").value = "";
}










/*  
@name= summarize
  @author= Estefani Paredes Valera
  @version= 1.0
  @description=This method is triggered when the user click the Introduce_the_products_in_the_Data_Base botton
               Call validate_letters_code
  @date = 4-11-2019
  @params= str_letter , arr_valid_letters
  @return = bool
*/
function summarize() {

    // validate_letters_code();
    var decision = confirm("Do you really want to introduce this clients?");
    if (decision) {
        window.open("../popUpWindows/popUpWindow.html", "blank", "width=400px, height=400px");
    }
}

