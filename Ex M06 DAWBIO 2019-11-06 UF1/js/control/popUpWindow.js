
/*  
@name= popUpWindow.js
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= controller associated to popUpWindow.html
  @date = 4-11-2019
  @params= none 
  @return = none 
*/


/*
@name= currentDate
@author= 
@version= 1.0
@description= function that prints today's date
@date = 
@params= none
@return = none
*/

function currentDate() {
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    var today = new Date();
    var date = days[today.getDay()] + ", " + today.getDate() + " of " + months[today.getMonth()] + ' ' + today.getFullYear();

    document.getElementById("data").innerHTML += date;
}

/*  
@name= loadSummary
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= event that captured when the pag is loades. It's used to hide/show some components.
                Create a table. 
  @date = 4-11-2019
  @params= none 
  @return = none 
*/

function loadSummary() {
    var frame = window.opener.document;
    var title = frame.getElementById("titleSelectClient").textContent;
    document.getElementById("titleSummary").innerText = title;
    console.log(title);
    currentDate();
    var numCli = frame.getElementById("number").textContent;
    console.log(numCli);//parseInt();
    let i = 0;
    var rowCode = "";
    var textChechbox = ""
    for (i; i < parseInt(numCli); i++) {
        if (frame.getElementById(i + "2").checked == true) {
            textChechbox = "YES";
        } else {
            textChechbox = "NO";
        }
        rowCode += "<tr id='" + i + "'>" +
            "<td>" + frame.getElementById(i + "0").value + "</td>" +
            "<td>" + frame.getElementById(i + "1").value + "</td>" +
            "<td>" + textChechbox + "</td>" +
            "</tr>";

    };
    document.getElementById("tableSummary").innerHTML += rowCode;
    document.getElementById("totalRow").innerText = "Total products introduced in the dataBase: " + String(i);

}


/*  
@name= printWindow
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the Print Window
  @date = 4-11-2019
  @params= none 
  @return = none 
*/
function printWindow() {
    window.print();
}

/*  
@name= closeWindow
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the close Window
  @date = 4-11-2019
  @params= none 
  @return = none 
*/
function closeWindow() {
    window.close();
}