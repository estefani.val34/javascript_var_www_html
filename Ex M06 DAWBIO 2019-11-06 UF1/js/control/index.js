/*  
@name= index.js
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= controller associated to index.html
  @date = 4-11-2019
  @params= none 
  @return = none 
*/
window.onload = function () {
    document.getElementById("divPrincipal").style.display = "block";
    document.getElementById("frame").style.display = "none";
    window.frames[0].document.getElementById("number").style.display = "none";



}

/*
@name= Vacio
@author= 
@version= 1.0
@description= Function that validate that contains a value
@date = 8-11-2018
@params= num
@return = boolean
*/
function Vacio(num) {
    if (num == '') {//If you do not enter anything
        document.getElementById("numberClients").innerHTML = "NO HAS INTRODUCIDO NADA, INTRODUCE UN NUMERO !!!";
        return false;
    }
    return true;
}

/*
@name= NoNumero
@author= 
@version= 1.0
@description= Function that validate that the value is a number
@date = 8-11-2018
@params= num
@return = boolean
*/
function NoNumero(num) {
    if (isNaN(num)) {//If it is not a number
        console.log("The data introduce is not a number");
        document.getElementById("pResult").innerHTML = "INTRODUCE UN NUMERO !!!";
        return false;
    }
    return true;
}


/*
@name= introduce
@author= Pablo Cubiles Cid
@version= 1.0
@description= open frame 
@date = 8-11-2018
@params= num
@return = boolean
*/
function introduce() {

    document.getElementById("divPrincipal").style.display = "none";
    document.getElementById("frame").style.display = "block";


    var selectType = document.getElementById("testingType").value;
    //console.log(selectType);
    window.frames[0].document.getElementById("titleSelectClient").innerText =
        "Data introduced for the insurance type:  " + selectType;

    var numberClient = document.getElementById("numberClients").value;
    window.frames[0].document.getElementById("number").innerText = numberClient;
    //console.log(numberClient);


    let i;
    let index = 0;
    var rowCode = "";
    for (i = 0; i < numberClient; i++) {
        index = i;

        rowCode += "<tr id='" + index + "'>" +
            " <td><input type='text' placeholder='name' id='" + i + 0 + "'></input> </td>" +
            "<td><input type='text' placeholder='surname' id='" + i + 1 + "'></input> </td>" +
            "<td> <input type='checkbox' id='" + i + 2 + "'></td>" +
            "</tr>";
    };
    window.frames[0].document.getElementById("mytable").innerHTML += rowCode;


}


