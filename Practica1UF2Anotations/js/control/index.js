$(document).ready(function () {
    $("#inputText").val("");
    $("#container2").hide();
    $("#error").hide();
    $("#no_error").hide();

    var idImg;

    /**
     * Resize the image if I place the mouse on top of the image
     */
    $(".chr").hover(function () {
        $(this).css("transform", "scale(1.2)");
    }, function () {
        $(this).css("transform", "scale(1)");
    });
    /**
     *Show  me div 2  where I can put DNA code,  when I click in to a picture 
     */
    $(".chr").click(function (event) {
        $("#container1").hide();
        $("#container2").show();
        idImg = $(this).attr('id');
        $(".titlenavbar2").attr("id", idImg);//I save it to retrieve the value later in the popup
        $(".titlenavbar2").append('<h4 class="navbar2" id="' + idImg + '" > DNA Analytic for chrmomosome number ' + idImg + '</h4>');
    });
    /**
     * Return to the content of main page when I click button back I 
     */
    $("#buttonBack").click(function () {
        $("#container2").hide();
        $("#container1").show();
        // $(".navbar2").removeAttr("style").hide();
        $(".navbar2").empty();
        $("#inputText").val("");
        $(".messageerror").removeAttr("style").hide();
        $(".valid").removeClass("valid");
        $(".invalid").removeClass("invalid");
    });
   
    /**
    * validate input, if is a empty string or nitrogen bases
    */
    $('#inputText').on('input ', function () {
        var inputtxt = $(this);
        var txt = inputtxt.val();
        var is_no_empty = txt;
        var re = /^[AGCTagct\n]+$/;
        var is_adn = re.test(txt);
        if (is_no_empty && is_adn) {//if is different to empty string , there are text
            inputtxt.removeClass("invalid").addClass("valid");
            $("#error").hide();
            $("#no_error").show();
        }
        else {
            inputtxt.removeClass("valid").addClass("invalid");
            $("#no_error").hide();
            $("#error").show();
        }
    });

    /**
    * Open PopUp Window when I clik the button Register Analysis
   */
    $("#buttonRegisterAnalysis").click(function () {
        //add text to textarea, and show html 
        //var text = $("#inputText").val(); 
        //$("#inputText").append(text);


        var valid = $("#inputText").hasClass("valid");
        if (valid) {
            var decision = confirm("Do you really want to introduce this data?");
            if (decision) {
                window.open("../Practica1UF2/popUpWindows/popUpWindow.html", "blank", "width=400px, height=400px");
            }
        } else {
            alert("ENTER A CORRECT CHAIN PLEASE");
        }

    });

});

