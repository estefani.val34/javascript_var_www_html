$(document).ready(function () {
    //console.log($(index).find("#title1"));  NO BORRAR
    //if I use append , call the text 
    //console.log($("#inputText",wod).html());
    //console.log($("#inputText",wod).text());

    var wod = window.opener.document;
    var text = $("#inputText", wod).val();
    var num_chr=$(".titlenavbar2", wod).attr('id');

    $("#divADN").append("chrmomosome number "+num_chr+"</br>");
    $("#divADN").append("DNA code: "+text.toUpperCase());
   
    
    /*  
    @name= 
    @author= Estefani Paredes Valera
    @version= 1.0
    @description= This method print the popUp Window when the user click Print button 
    @date = 21-11-2019
    @params= none 
    @return = none 
    */
    $("#print").click(function () {
        window.print();
    });

    /*  
    @name= 
    @author= Estefani Paredes Valera
    @version= 1.0
    @description= This method is close Window when the user click close Window button
    @date = 21-11-2019
    @params= none 
    @return = none 
    */
    $("#closeWindow").click(function () {
        window.close();
    });
});