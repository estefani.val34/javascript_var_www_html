
/**
* @name = Practica1
* @author = Ankush Rana
* @version = v1.0.0
* @description = Event that capture when the page is loaded.
*                It's used to call a method to hide/show some components.
* @date = 2-10-2019
*/
window.onload = function () {
    this.showDiv()
}

/**
 * @name    showDiv
 * @author  Ankush
 * 
 * @since	v0.0.1
 * @version v1.0.0  Saturday, October 12th, 2019.
 * @description Method to hide all divs and show just one by id received as parameter 
 * @date = 12-10-2019
 * @param	{String}    exercise_id	Default: null
 * @return	void
 */
function showDiv(exercise_id = null) {

    exerciseDivsLength = document.getElementsByClassName("exerciseDiv").length
    for (let i = 0; i < exerciseDivsLength; i++) {
        document.getElementsByClassName("exerciseDiv")[i].style.display = "none";
    }
    if (exercise_id != null) {
        document.getElementById(exercise_id).style.display = "block";
    }
    document.getElementById("resultEx1").style.display = "none";
}

/////////////////////////////////////First exercise/////////////////////////////////////

/**
 * @name    firstExercise
 * @author	Ankush Rana
 * 
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Method to start the first exercise.
 * @date    12-10-2019
 * @return	void
 */
function firstExercise() {
    showDiv("FA111");
    document.getElementById("resultTextEx1").classList.remove("text-primary");
    document.getElementById("resultTextEx1").classList.remove("text-danger");
    document.getElementById("resultTextEx1").classList.remove("text-success");
    document.getElementById("playEx1").style.display = "block";
    document.getElementById("resultEx1").style.display = "none";
}

/**
 * @name    checkPrimeNumber
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Method to check prime number.
 * @date    12-10-2019
 * @param	mixed	number	
 * @return	boolean
 */
function checkPrimeNumber(number) {
    if (number < 2) {
        document.getElementById("resultTextEx1").classList.add("text-primary");
        document.getElementById("resultTextEx1").innerText = number + " is neither a prime nor a composite number";
        return false;
    } else if (number == 2) {
        document.getElementById("resultTextEx1").classList.add("text-success");
        document.getElementById("resultTextEx1").innerText = number + " is a prime number.";
        return true;
    } else {
        for (let i = 3; i < number; i += 2) {
            if (number % i == 0) {
                document.getElementById("resultTextEx1").classList.add("text-primary");
                document.getElementById("resultTextEx1").innerText = number + " is a composite number.";
                return false;
            }
        }
    }
    document.getElementById("resultTextEx1").classList.add("text-success");
    document.getElementById("resultTextEx1").innerText = number + " is a prime number.";
    return true;
}

/**
 * @name validNumberEx1
 * 
 * @author	Ankush Rana
 * 
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Method to valid a number between 1 and 10000.
 * @date    12-10-2019
 * @param	mixed	number	
 * @return	boolean
 */
function validNumberEx1(number) {

    if (isNaN(number) == false && number != "") {
        if (number >= 0 && number <= 10000) {
            return true;
        } else {
            errorString = "The number must be between 0 and 10000.";
        }
    } else if (number == "") {
        errorString = "The input field is empty.";
    } else {
        errorString = "Please make sure, it is a number.";
    }
    document.getElementById("resultTextEx1").innerText = errorString;
    return false;
}

/**
 * @name    PlayFirstExercise
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @discriptrion    Method for submit first exercise. 
 *                  It works as main.
 * @date = 12-10-2019
 * @return	void
 */
function PlayFirstExercise() {
    inputValue = document.getElementById("inputNum_FA111").value.toString().trim();
    document.getElementById("inputNum_FA111").value = "";

    if (validNumberEx1(inputValue)) {

        checkPrimeNumber(inputValue);

    } else {
        document.getElementById("resultTextEx1").classList.add("text-danger");
    }

    document.getElementById("playEx1").style.display = "none";
    document.getElementById("resultEx1").style.display = "block";
}


/////////////////////////////////////Second exercise/////////////////////////////////////

// Global variables. 
var randomNum = null; // Random number it will create just ones, during staring the exercise 2.
var minValue = null; // minimum value of numbers, the number can't be less the this value.
var maxValue = null; // Maximum value, the number musn't be greater than it.
var countTurns = null; // A variable for counting all round.
var totalTurns = null; // Total rounds to play.
var winner = null; // If the use wins, it will start with value False, and when the user wins it will change to true.

/**
 * @name resetEx2
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Method to reset the exercise 2.
 *              It will reset html to it's origen value.
 * @date    12-10-2019
 * @return	void
 */
function resetEx2() {
    document.getElementById("playDivTextEx2").classList.remove("text-danger");
    document.getElementById("playDivTextEx2").classList.remove("text-primary");
    document.getElementById("playDivTextEx2").innerText = "";

    document.getElementById("inputNum_FA112").value = ""

    document.getElementById("totalTermsTextEx2").classList.remove("text-primary");
    document.getElementById("totalTermsTextEx2").classList.remove("text-danger");
    document.getElementById("totalTermsTextEx2").innerText = "";

    document.getElementById("restart").style.display = "none";
    document.getElementById("inputNum_FA112").style.display = "block";
    document.getElementById("secondExSubmit").style.display = "block";


    document.getElementById("playDivEx2").style.display = "block";
    document.getElementById("resultDivEx2").style.display = "none";
    document.getElementById("resultDivEx2Txt").classList.remove("text-danger");
    document.getElementById("resultDivEx2Txt").classList.remove("text-primary");
    showDiv("FA112");
}


/**
 * @name    checkdNumberEx2
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Function to valid number. It will check, if it's a number and also if the number matchs with secret number.
 *              All message will be apeared depending on each condition. 
 * @date 12-10-2019
 * @param	mixed	number	
 * @return	boolean
 */
function checkdNumberEx2(number) {
    let elementPlayDivTextEx2 = document.getElementById("playDivTextEx2");
    if (isNaN(number) == false && number != "") {
        if (inputValue > maxValue || inputValue < minValue) {
            elementPlayDivTextEx2.classList.add("text-danger");
            elementPlayDivTextEx2.innerText = "The number is out of the range.";
            countTurns -= 1;
        } else if (inputValue > randomNum) {
            elementPlayDivTextEx2.classList.add("text-danger")
            elementPlayDivTextEx2.innerText = "Wrong answer!\nThe number is less than your's.";
        } else if (inputValue < randomNum) {
            elementPlayDivTextEx2.classList.add("text-danger")
            elementPlayDivTextEx2.innerText = "Wrong answer!\nThe number is greater than your's.";
        } else {
            return true
        }
    } else {
        countTurns -= 1;
        elementPlayDivTextEx2.classList.add("text-danger")
        if (number == "") {
            elementPlayDivTextEx2.innerText = "The input is empty.";
        } else {
            elementPlayDivTextEx2.innerText = "You must enter a number.";
        }
    }
    return false;
}


/**
 * @name    printTurns
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Method to print rounds left for user.
 * @date 12-10-2019
 * @return	void
 */
function printTurns() {

    document.getElementById("totalTermsTextEx2").classList.add("text-primary");
    if ((totalTurns - countTurns) <= 1) {
        roundWord = "round"
    } else {
        roundWord = "rounds"
    }
    if (countTurns == 0) {
        document.getElementById("totalTermsTextEx2").innerText = "You have total " + (totalTurns - countTurns) + " " + roundWord + ".";

    } else {
        document.getElementById("totalTermsTextEx2").innerText = "You have " + (totalTurns - countTurns) + " " + roundWord + " left.";
    }
}

/**
 * @name secondExercise
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Methot start the exercise. Here all global varible will get values as minvalue, maxValue, winner, etc.
 * @date    12-10-2019
 * @return	void
 */
function secondExercise() {
    minValue = 0;
    maxValue = 500;
    countTurns = 0;
    totalTurns = 50;
    winner = false;
    randomNum = Math.floor(Math.random() * maxValue + minValue)
    resetEx2();
    printTurns()

}

/**
 * @name    playSecondExercise
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method works when the user click to submit the excercise. It works as main.
 * @data 12-10-2019
 * @return	void
 */
function playSecondExercise() {
    inputValue = document.getElementById("inputNum_FA112").value.toString().trim();
    resetEx2();
    countTurns += 1;
    winner = checkdNumberEx2(inputValue);

    if ((countTurns == totalTurns) || winner) {
        document.getElementById("playDivEx2").style.display = "none";
        document.getElementById("resultDivEx2").style.display = "block";

        if (countTurns == totalTurns) {
            document.getElementById("resultDivEx2Txt").classList.add("text-danger");
            document.getElementById("resultDivEx2Txt").innerText = "Please restart the game, you have reached your maximum number of failed attempts.";

        } else if (winner) {
            document.getElementById("resultDivEx2Txt").classList.add("text-primary");
            document.getElementById("resultDivEx2Txt").innerText = "Congratulations!\nYou've found the number " + inputValue + ".\nTotal turns: " + (countTurns);
        }
        document.getElementById("restart").style.display = "block";
        document.getElementById("secondExSubmit").style.display = "none";
        document.getElementById("inputNum_FA112").style.display = "none";
    }
    printTurns()
}


////////////////////////////////////////////Excercise3//////////////////////////////////////////

// All constant needed for exercise 3
const add = (a, b) => a + b;
const multiply = (a, b) => a * b;

const minValueEX3 = 1;
const maxValueEX3 = 2000;
const devisorEx3 = 13;

/**
 * @name    resetEx3.
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Method to reset th exercise 3, it will reset the exercise to it's orignal value.
 * @date    12-10-2019
 * @return	void
 */
function resetEx3() {
    document.getElementById("resultDivEx3").style.display = "none";
    document.getElementById("playDivEx3").style.display = "block";

    document.getElementById("productTextEx3").innerText = "";
    document.getElementById("sumTextEx3").innerText = "";

    let table = document.getElementById("tableEx3")
    console.log(table)
    if (table) {
        table.parentNode.removeChild(table);
    }
}

/**
 * @name    thirdExcercise
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description Method to open the exercise. It work when the user open the exercise 3
 * @date 12-10-2019
 * @return	void
 */
function thirdExcercise() {
    resetEx3()
    showDiv("FA113");
}

/**
 * @name    devisableNumbersIter
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The iterable Method to create iterable list of all numbers divisable by 13, between 1 to 2000.
 * @date 12-10-2019
 * @param	{int}	min    	
 * @param	{int}	max    	
 * @param	{int}	divisor	
 * @return	void
 */
function* devisableNumbersIter(min, max, divisor) {
    while (min <= max) {
        if (min % divisor == 0) {
            yield min;
        }
        min++
    }
}

/**
 * @name    createTableEx3
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description This method create a table with list of numbers, list must be iterable. 
 * @date 12-10-2019
 * @param	{iterable}	numberList	
 * @return	void
 */
function createTableEx3(numberList) {

    let tableDiv = document.getElementById("tableDivEx3");
    let table = document.createElement("TABLE");
    let trTittle = document.createElement("TR");
    trTittle.setAttribute("id", "trTittle");
    let thTittle = document.createElement("TH");
    let textNode = document.createTextNode("List of all divisable number by 13, between " + minValue + " and " + maxValueEX3 + ":");


    thTittle.appendChild(textNode);
    thTittle.setAttribute("colspan", "4")
    trTittle.appendChild(thTittle);
    table.append(trTittle);

    table.setAttribute("class", "table table-dark");
    table.setAttribute("id", "tableEx3");
    tableDiv.append(table);

    for (let number of numberList) {
        let tr = document.createElement("TR");

        let td1 = document.createElement("TD");
        let td2 = document.createElement("TD");
        let td3 = document.createElement("TD");
        let td4 = document.createElement("TD");


        textNode1 = document.createTextNode(number);
        td1.appendChild(textNode1)
        tr.appendChild(td1)

        number2 = numberList.next().value
        number3 = numberList.next().value
        number4 = numberList.next().value


        if (number2 != undefined) {
            textNode2 = document.createTextNode(number2);
            td2.appendChild(textNode2);
            tr.appendChild(td2)
        }
        if (number3 != undefined) {
            textNode3 = document.createTextNode(number3);
            td3.appendChild(textNode3);
            tr.appendChild(td3);
        }
        if (number4 != undefined) {
            textNode4 = document.createTextNode(number4);
            td4.appendChild(textNode4);
            tr.appendChild(td4);
        }
        table.appendChild(tr)
    }
    document.getElementById("trTittle").classList.add("bg-primary")
    document.getElementById("trTittle").style.textAlign = "center"
    document.getElementById("tableEx3").style.textAlign = "center"

}

/**
 * @name    result_product_sum
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method is to get the sum and products of numbers as well as print them on html page. 
 * @date 12-10-2019
 * @param	{list}	iterNumbers_for_list    It must be list of numbers.
 * @return	void
 */
function result_product_sum(iterNumbers_for_list) {

    let numbers = Array.from(iterNumbers_for_list);

    let sum = numbers.reduce(add);
    let product = numbers.reduce(multiply);

    if (product == Infinity) {
        productResultString = "Product: <span style='color:red'>(Error out of limit)</span> It can't calculate product of all number. The result is out of limit."
    } else {
        productResultString = "Product: " + product;
    }
    document.getElementById("productTextEx3").innerHTML = productResultString;
    document.getElementById("sumTextEx3").innerText = "Sum: " + sum;
}

/**
 * @name    playThirdExercise
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method to play exercise 3. It work when the user submit it. In addition works as main.
 * @date 12-10-2019
 * @return	void
 */
function playThirdExercise() {
    const minValueEX3 = 1;
    const maxValueEX3 = 2000;
    const devisorEx3 = 13;
    document.getElementById("resultDivEx3").style.display = "block";
    document.getElementById("playDivEx3").style.display = "none";

    iterNumbers = devisableNumbersIter(minValueEX3, maxValueEX3, devisorEx3);
    createTableEx3(iterNumbers)

    iterNumbers_for_list = devisableNumbersIter(minValueEX3, maxValueEX3, devisorEx3);
    result_product_sum(iterNumbers_for_list);

}

////////////////////////////////////// EX4 ///////////////////////////////////

/**
 * @name    resetStylesEx4
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method to reset all style of the exercise 4 to it's orignal value.
 * @date    12-10-2019
 * @return	void
 */
function resetStylesEx4() {
    document.getElementById("errorDivEx4").style.visibility = "hidden";
    document.getElementById("errorTextEx4").innerText = ".";

    document.getElementById("firstNumberEx4").style.background = "white"
    document.getElementById("secondNumberEx4").style.background = "white"
    document.getElementById("operatorEx4").style.background = "white"
}

/**
 * @name    forthExcercise
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method to start the exercise 4. It start all value as empty of inputs. And hide the reset button
 * @date 12-10-2019
 * @return	void
 */
function forthExcercise() {
    document.getElementById("firstNumberEx4").value = "";
    document.getElementById("secondNumberEx4").value = "";
    document.getElementById("resultEx4").value = "";
    document.getElementById("operatorEx4").value = "";

    document.getElementById("resetEx4").style.display = "none";
    document.getElementById("calculateEx4").style.display = "block";

    resetStylesEx4();
    showDiv("FA114");
}

/**
 * @name    validNumberEx4
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method valid a number. If it's a number then returns true. Otherwise return false.
 * @date    12-10-2019
 * @param	{mixed}	number	
 * @return	boolean
 */
function validNumberEx4(number) {
    if (isNaN(number) == false && number != "") {
        return true;
    }
    return false;
}

/**
 * @name    validOperatorEx4
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method valid the operators (* ,/ , -, +) and return true, if matchs any. Otherwise returns false.
 * @date    12-10-2019
 * @param	{string}	operator	
 * @return	boolean
 */

function validOperatorEx4(operator) {
    if (Array("*", "/", "-", "+").includes(operator)) {
        return true;
    }
    return false;
}

/**
 * @name    calculateEx4
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @global
 * @param	mixed	number1	
 * @param	mixed	number2	
 * @param	mixed	obretor	
 * @return	void
 */

function calculateEx4(number1, number2, obretor) {
    switch (operator) {
        case "*": return number1 * number2;
        case "/": return number1 / number2;
        case "-": return number1 - number2;
        case "+": return number1 + number2;
    }
}

/**
 * @name    playForthExercise
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method work as main for exercise 4. It works when a user click to submit on this exercise.
 * @date
 * @return	void
 */
function playForthExercise() {
    resetStylesEx4();
    number1 = document.getElementById("firstNumberEx4").value;
    number2 = document.getElementById("secondNumberEx4").value;
    operator = document.getElementById("operatorEx4").value.toString().trim();
    errorFound = false;

    if (!validNumberEx4(number1)) {
        document.getElementById("firstNumberEx4").style.background = "red"
        errorFound = true;
    }
    if (!validNumberEx4(number2)) {
        document.getElementById("secondNumberEx4").style.background = "red"
        errorFound = true;
    }
    if (!validOperatorEx4(operator)) {
        document.getElementById("operatorEx4").style.background = "red"
        errorFound = true;
    }
    if (errorFound) {
        document.getElementById("errorDivEx4").style.visibility = "visible";
        document.getElementById("errorTextEx4").style.color = "red";
        document.getElementById("errorTextEx4").innerText = "Incorrect data, please check the data."
        return false;
    }

    result = calculateEx4(number1, number2, operator)
    if (result == Infinity) {
        document.getElementById("resultEx4").style.color="red";
        document.getElementById("resultEx4").value = "Error (Infinity): It's out of limit can't calculate.";
    }else{
        document.getElementById("resultEx4").value = result;
    }
    // document.getElementById("calculateEx4").style.display = "none"; //Better no hide this button.
    document.getElementById("resetEx4").style.display = "block";
}

/**
 * @name    backInput1Ex4
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method to change the background of input, to value as white. It work when a user focas on the input, in this case (id = firstNumberEx4)
 * @date 12-10-2019
 * @return	void
 */
function backInput1Ex4() {
    document.getElementById("errorDivEx4").style.visibility = "hidden";
    document.getElementById("firstNumberEx4").style.background = "white";

}

/**
 * @name    backInput2Ex4
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method to change the background of input, to value as white. It work when a user focas on the input, in this case (id = operatorEx4)
 * @date 12-10-2019
 * @return	void
 */
function backInput2Ex4() {
    document.getElementById("errorDivEx4").style.visibility = "hidden";
    document.getElementById("operatorEx4").style.background = "white";
}

/**
 * @name    backInput3Ex4
 *
 * @author	Ankush Rana
 * @since	v0.0.1
 * @version	v1.0.0	Saturday, October 12th, 2019.
 * @description The method to change the background of input, to value as white. It work when a user focas on the input, in this case (id = secondNumberEx4)
 * @date 12-10-2019
 * @return	void
 */
function backInput3Ex4() {
    document.getElementById("errorDivEx4").style.visibility = "hidden";
    document.getElementById("secondNumberEx4").style.background = "white";
}