/*  
@name= index.js
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= controller associated to index.html
  @date = 2-10-2019
  @params= none 
  @return = none 
*/



var attempts = 0;
const allowed_attempts = 50;
var secret_number = Math.floor(Math.random() * 500) + 1;


const multiples_of_the_number = 13;
const max_num_can_multiply = 2000;
var product = 1;
var sumar = 0;


/*  
@name= window.onload
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= event that captured when the pag is loades. It's used to hide/show some components
  @date = 2-10-2019
  @params= none 
  @return = none 
*/


window.onload = function () {
  document.getElementById("buttonFA111").style.display = "block";
  document.getElementById("butttonFA112").style.display = "block";
  document.getElementById("butttonFA113").style.display = "block";
  document.getElementById("butttonFA114").style.display = "block";
  document.getElementById("FA111").style.display = "none";
  document.getElementById("FA112").style.display = "none";
  document.getElementById("FA113").style.display = "none";
  document.getElementById("FA114").style.display = "none";
}


//FA111

/*  
@name= clickBUtton
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the FA111 button
  @date = 2-10-2019
  @params= none 
  @return = none 
*/


function clickButton() {
  document.getElementById("FA111").style.display = "block";
  document.getElementById("butttonFA112").style.display = "none";
  document.getElementById("butttonFA113").style.display = "none";
  document.getElementById("butttonFA114").style.display = "none";
  document.getElementById("divResult").style.display = "none";

}


/*  
@name= clickButtonCalculate
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the Calculate button. 
                Calculate if a number is prime or not and if it is out of range
  @date = 2-10-2019
  @params= none 
  @return = none 
*/


function clickButtonCalculate() {
  document.getElementById("divIntro").style.display = "none";
  document.getElementById("divResult").style.display = "block";
  //console.log (document.getElementById("divIntro") );
  //console.log( document.getElementById("txtNumber").value);
  var result = document.getElementById("txtNumber").value;
  var cont = 0;

  // numero primo si es dibisible por 1 y el mismo 
  //-Si en contador es mayor a 2, no es primo

  if (isNaN(result) == false) {

    if (result >= 0 && result <= 10000) {
      // console.log("miraremos si es primo ");
      document.getElementById("outofrange").style.display = "none";
      for (let index = 1; index <= result; index++) {

        if (result % index == 0) {
          cont++;
        }
      }

      if (cont > 2) {
        // console.log("no es primo");
        document.getElementById("prime").style.display = "none";
        document.getElementById("notprime").style.display = "block";
        document.getElementById("errorNANFA111").style.display = "none";


      } else {
        //console.log("es primo");
        document.getElementById("prime").style.display = "block";
        document.getElementById("notprime").style.display = "none";
        document.getElementById("errorNANFA111").style.display = "none";
      }


    } else {
      // console.log("fuera de rango ");
      document.getElementById("outofrange").style.display = "block";
      document.getElementById("notprime").style.display = "none";
      document.getElementById("prime").style.display = "none";
      document.getElementById("errorNANFA111").style.display = "none";
    }

  } else {
    document.getElementById("errorNANFA111").style.display = "block";
    document.getElementById("outofrange").style.display = "none";
    document.getElementById("notprime").style.display = "none";
    document.getElementById("prime").style.display = "none";
  }


}

/*  
@name= clickBack
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the back button, It hides/show some components.
  @date = 2-10-2019
  @params= none 
  @return = none 
*/


function clickBack() {
  document.getElementById("FA111").style.display = "none";
  document.getElementById("butttonFA112").style.display = "block";
  document.getElementById("butttonFA113").style.display = "block";
  document.getElementById("butttonFA114").style.display = "block";
}



/*  
@name= clickBack2
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the back button, It hides/show some components.
  @date = 2-10-2019
  @params= none 
  @return = none 
*/



function clickBack2() {
  document.getElementById("divIntro").style.display = "block";
  document.getElementById("divResult").style.display = "none";
}



//FA112

/*  
@name= clickButtonFA112
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the FA112 button
  @date = 2-10-2019
  @params= none 
  @return = none 
*/


function clickButtonFA112() {
  document.getElementById("buttonFA111").style.display = "none";
  document.getElementById("butttonFA112").style.display = "block";
  document.getElementById("butttonFA113").style.display = "none";
  document.getElementById("butttonFA114").style.display = "none";
  document.getElementById("FA112").style.display = "block";
  document.getElementById("divResultFA112").style.display = "none";

}



/*  
@name= clickButtonCalculateFA112
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the Calculate button. 
                You have to guess the random number;
 @date = 2-10-2019
  @params= none 
  @return = none 
*/



function clickButtonCalculateFA112() {

  document.getElementById("divResultFA112").style.display = "block";
  var entered_number = document.getElementById("txtNumberFA112").value;

  /*
    console.log("numero secreto:");
    console.log(secret_number);*/


  attempts = attempts + 1;

  if (isNaN(entered_number) == false) {

    if (entered_number >= 1 && entered_number <= 500) {
      if (attempts <= allowed_attempts) {

        if (entered_number == secret_number) {
          document.getElementById("guessed").style.display = "block";
          document.getElementById("smaller").style.display = "none";
          document.getElementById("bigger").style.display = "none";
          document.getElementById("maxAttempts").style.display = "none";
          document.getElementById("errorNANFA112").style.display = "none";
          document.getElementById("outofrangeFA112").style.display = "none";
        } else if (entered_number > secret_number) {
          document.getElementById("smaller").style.display = "block";
          document.getElementById("guessed").style.display = "none";
          document.getElementById("bigger").style.display = "none";
          document.getElementById("maxAttempts").style.display = "none";
          document.getElementById("errorNANFA112").style.display = "none";
          document.getElementById("outofrangeFA112").style.display = "none";
        } else if (entered_number < secret_number) {
          document.getElementById("bigger").style.display = "block";
          document.getElementById("guessed").style.display = "none";
          document.getElementById("smaller").style.display = "none";
          document.getElementById("maxAttempts").style.display = "none";
          document.getElementById("errorNANFA112").style.display = "none";
          document.getElementById("outofrangeFA112").style.display = "none";
        }

      } else {
        // console.log("Has superado el numera máximo de intentos ");
        document.getElementById("maxAttempts").style.display = "block";
        document.getElementById("bigger").style.display = "none";
        document.getElementById("guessed").style.display = "none";
        document.getElementById("smaller").style.display = "none";
        document.getElementById("errorNANFA112").style.display = "none";
        document.getElementById("outofrangeFA112").style.display = "none";
      }
    } else {
      document.getElementById("outofrangeFA112").style.display = "block";
      document.getElementById("maxAttempts").style.display = "none";
      document.getElementById("bigger").style.display = "none";
      document.getElementById("guessed").style.display = "none";
      document.getElementById("smaller").style.display = "none";
      document.getElementById("errorNANFA112").style.display = "none";

    }



  } else {
    document.getElementById("errorNANFA112").style.display = "block";
    document.getElementById("maxAttempts").style.display = "none";
    document.getElementById("bigger").style.display = "none";
    document.getElementById("guessed").style.display = "none";
    document.getElementById("smaller").style.display = "none";
    document.getElementById("outofrangeFA112").style.display = "none";

  }

}


/*  
@name= clickBackFA112
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the back button. Return to the home page.
  @date = 2-10-2019
  @params= none 
  @return = none 
*/



function clickBackFA112() {

  document.getElementById("buttonFA111").style.display = "block";
  document.getElementById("butttonFA112").style.display = "block";
  document.getElementById("butttonFA113").style.display = "block";
  document.getElementById("butttonFA114").style.display = "block";
  document.getElementById("FA111").style.display = "none";
  document.getElementById("FA112").style.display = "none";

}


//FA113

/*  
@name= clickButtonFA113
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the FA113 button
  @date = 2-10-2019
  @params= none 
  @return = none 
*/

function clickButtonFA113() {
  document.getElementById("buttonFA111").style.display = "none";
  document.getElementById("butttonFA112").style.display = "none";
  document.getElementById("butttonFA113").style.display = "block";
  document.getElementById("butttonFA114").style.display = "none";
  document.getElementById("FA113").style.display = "block";

}

/*  
@name= clickButtonCalculateFA113
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the Calculate button. 
                Calculate the multiples of 13, the sum and the product of all of them;
 @date = 2-10-2019
  @params= none 
  @return = none 
*/

function clickButtonCalculateFA113() {

  var text = "";

  var i;

  for (i = 1; i <= max_num_can_multiply; i++) {

    text += multiples_of_the_number + " x " + i + " = " + i * multiples_of_the_number + "<br>";

    product = product * i * multiples_of_the_number;

    sumar = sumar + i * multiples_of_the_number;

  }


  document.getElementById("demo").innerHTML = text;

  document.getElementById("sum").innerText = "The sum of all numbers is: " + sumar;

  if (product == "Infinity") {
    document.getElementById("multiplication").innerText = "The multiplication of all numbers is: " + "OUT OF RANGE";
  } else {
    document.getElementById("multiplication").innerText = "The multiplication of all numbers is: " + product;
  }

}


/*  
@name= clickBackFA113
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the back button. Return to the home page.
  @date = 2-10-2019
  @params= none 
  @return = none 
*/



function clickBackFA113() {

  document.getElementById("buttonFA111").style.display = "block";
  document.getElementById("butttonFA112").style.display = "block";
  document.getElementById("butttonFA113").style.display = "block";
  document.getElementById("butttonFA114").style.display = "block";
  document.getElementById("FA111").style.display = "none";
  document.getElementById("FA112").style.display = "none";
  document.getElementById("FA113").style.display = "none";

}




//FA114

/*  
@name= clickButtonFA114
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the FA114 button
  @date = 2-10-2019
  @params= none 
  @return = none 
*/

function clickButtonFA114() {
  document.getElementById("buttonFA111").style.display = "none";
  document.getElementById("butttonFA112").style.display = "none";
  document.getElementById("butttonFA113").style.display = "none";
  document.getElementById("butttonFA114").style.display = "block";
  document.getElementById("FA114").style.display = "block";

}


/*  
@name= clickButtonCalculateFA113
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the Calculate button. 
                Calculate the multiples of 13, the sum and the product of all of them;
 @date = 2-10-2019
  @params= none 
  @return = none 
*/

function clickButtonCalculateFA114() {

  var first, second;
  var operand;
  var resultCalculator;

  first = document.getElementById("txtFirstNumberFA114").value;
  second = document.getElementById("txtSeconNumberFA114").value;
  operand = document.getElementById("txtOperationFA114").value;
 
  if (isNaN(first) == false && isNaN(second) == false && ((operand == "x") == true || (operand == "/") == true || (operand == "+") == true || (operand == "-") == true)) {


    if ((operand == "x") == true) {
      resultCalculator = first * second;

    } else if ((operand == "/") == true) {
      resultCalculator = first / second;

    } else if ((operand == "+") == true) {
      resultCalculator = parseFloat(first) + parseFloat(second);

    } else if ((operand == "-") == true) {
      resultCalculator = first - second;
    }


  } else {
    resultCalculator = "Input NO valid!!";
  }

   document.getElementById("demoFA114").innerHTML ="Result :"+"<br>"+ resultCalculator;
}



/*  
@name= clickBackFA114
  @author= Estefani Paredes Valera
  @version= 1.0
  @description= This method is triggered when the user click the back button. Return to the home page.
  @date = 2-10-2019
  @params= none 
  @return = none 
*/

function clickBackFA114() {

  document.getElementById("buttonFA111").style.display = "block";
  document.getElementById("butttonFA112").style.display = "block";
  document.getElementById("butttonFA113").style.display = "block";
  document.getElementById("butttonFA114").style.display = "block";
  document.getElementById("FA111").style.display = "none";
  document.getElementById("FA112").style.display = "none";
  document.getElementById("FA113").style.display = "none";
  document.getElementById("FA114").style.display = "none";

}





